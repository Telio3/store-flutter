class RateModel {
  final double rate;
  final int count;

  RateModel(this.rate, this.count);

  factory RateModel.fromJson(Map<String, dynamic> json) {
    return RateModel(
      double.parse(json['rate'].toString()),
      json['count'] as int,
    );
  }
}