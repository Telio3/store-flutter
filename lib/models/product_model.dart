import 'package:store/models/rate_model.dart';

class ProductModel {
  final int id;
  final String title;
  final double price;
  final String description;
  final String category;
  final String image;
  final RateModel rating;

  ProductModel(this.id, this.title, this.price, this.description, this.category, this.image, this.rating);

  factory ProductModel.fromJson(Map<String, dynamic> json) {
    return ProductModel(
      json['id'] as int,
      json['title'] as String,
      double.parse(json['price'].toString()),
      json['description'] as String,
      json['category'] as String,
      json['image'] as String,
      RateModel.fromJson(json['rating']),
    );
  }

  toJson() {
    return {
      'id': id,
      'title': title,
      'image': image,
      'price': price,
    };
  }
}
