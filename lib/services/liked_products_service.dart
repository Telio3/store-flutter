
import 'package:hive/hive.dart';
import 'package:store/models/product_model.dart';

class LikedProductsService {
  static late Box<dynamic> box;

  static Future<void> onLikeProduct(ProductModel product) async {
    await box.put(
      product.id.toString(),
      product.toJson(),
    );
  }

  static onUnlikeProduct(ProductModel product) {
    box.delete(product.id.toString());
  }

  static Iterable<dynamic> getLikedProducts() {
    return box.values;
  }
}