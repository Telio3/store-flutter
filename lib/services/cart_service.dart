
import 'package:hive/hive.dart';
import 'package:store/models/product_model.dart';

class CartService {
  static late Box<dynamic> box;

  static Future<void> onAddCart(ProductModel product) async {
    dynamic productInCart = box.get(product.id.toString());

    if (productInCart != null) {
      updateProduct(product, productInCart['quantity'] + 1);
    } else {
      await box.put(
        product.id.toString(),
        {...product.toJson(), 'quantity': 1},
      );
    }
  }

  static Iterable<dynamic> getCartProducts() {
    return box.values;
  }

  static num getQuantity() {
    num quantity = 0;
    for (var element in box.values) {
      quantity += element['quantity'];
    }
    return quantity;
  }
  
  static num getTotalPrice() {
    num totalPrice = 0;
    for (var element in box.values) {
      totalPrice += element['price'] * element['quantity'];
    }
    return totalPrice;
  }

  static Future<void> removeProduct(String id) async {
    await box.delete(id);
  }

  static Future<void> updateProduct(product, num value) async {
    await box.put(
      product['id'].toString(),
      {...product, 'quantity': value},
    );
  }
}