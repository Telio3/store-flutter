import 'package:flutter/material.dart';

class CreateTextField {
  static Column createTextField(String label, TextInputType textInputType, TextEditingController controller) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10.0, top: 10.0),
          child: Text(
            label,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
        ),
        TextField(
          controller: controller,
          keyboardType: textInputType,
          cursorColor: Colors.red,
          autofocus: false,
          style: const TextStyle(fontSize: 15.0, color: Colors.black),
          decoration: InputDecoration(
            filled: true,
            fillColor: Colors.black12,
            border: InputBorder.none,
            contentPadding: const EdgeInsets.only(
                left: 14.0, bottom: 6.0, top: 8.0),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide.none
            ),
            enabledBorder: UnderlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide.none
            ),
          ),
        ),
      ],
    );
  }
}