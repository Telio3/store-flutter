import 'package:flutter/material.dart';
import 'package:store/screens/page_views/cart/cart_form_screen.dart';
import 'package:store/screens/page_views/cart/cart_list_screen.dart';
import 'package:store/screens/page_views/cart/cart_payment_screen.dart';
import 'package:store/services/cart_service.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  final List<dynamic> likedProducts = CartService.getCartProducts().toList();

  final PageController _pageController = PageController(
    initialPage: 0
  );

  int _currentStep = 0;

  void tapped(int step) {
    setState(() => _currentStep = step);
  }

  void continued() {
    _currentStep < 2 ?
        setState(() => _currentStep += 1): null;
    _pageController.animateToPage(_currentStep, duration: const Duration(milliseconds: 300), curve: Curves.easeInOut);
  }

  static const List<IconData> _icons = [
    Icons.shopping_bag,
    Icons.location_city,
    Icons.payment,
  ];

  List<Widget> _indicators(imagesLength, currentIndex) {
    return List<Widget>.generate(imagesLength, (index) {
      return SizedBox(
        height: 30,
        child: CircleAvatar(
          backgroundColor: currentIndex == index ? Colors.red : Colors.black26,
          child: Icon(
            _icons[index],
            color: Colors.white,
            size: 20.0,
          ),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: _indicators(3, _currentStep),
            ),
          ),
          Expanded(
            child: PageView(
              controller: _pageController,
              onPageChanged: (index) => tapped(index),
              children: const <Widget>[
                CartListScreen(),
                CartFormScreen(),
                CartPaymentScreen()
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
                  minimumSize: MaterialStateProperty.all(const Size(250.0, 50.0)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                  ),
                ),
                onPressed: () => continued(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      _currentStep < 2 ? 'Continue' : 'Pay',
                      style: const TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const Icon(
                      Icons.arrow_forward_ios,
                      size: 30.0,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}