import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:store/screens/account_screen.dart';
import 'package:store/screens/cart_screen.dart';
import 'package:store/screens/favorite_screen.dart';
import 'package:store/screens/products_list_screen.dart';
import 'package:store/services/cart_service.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final PageController _pageController = PageController(initialPage: 0);
  int _currentIndex = 0;

  static const List<Widget> _widgetOptions = <Widget>[
    ProductsListScreen(),
    FavoriteScreen(),
    CartScreen(),
    AccountScreen(),
  ];

  static const List<String> _titles = [
    'Catalog',
    'Favorites',
    'Cart',
    'Account',
  ];

  void _onItemTapped(int index) {
    setState(() {
      _currentIndex = index;
      _pageController.animateToPage(index, duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_titles.elementAt(_currentIndex)),
        backgroundColor: Colors.red,
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Badge(
              position: BadgePosition.topEnd(top: 0, end: 0),
              badgeColor: Colors.white,
              badgeContent: Text(CartService.getQuantity().toString()),
              child: IconButton(
                icon: const Icon(Icons.shopping_cart),
                tooltip: 'Show Snackbar',
                onPressed: () {
                  _onItemTapped(2);
                },
              ),
            ),
          ),
        ],
      ),

      body: PageView(
          controller: _pageController,
          onPageChanged: (index) {
            setState(() => _currentIndex = index);
          },
          children: _widgetOptions,
        ),
      
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: _onItemTapped,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        unselectedItemColor: Colors.grey,
        selectedItemColor: Colors.red,
        type: BottomNavigationBarType.fixed,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home_rounded),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite_rounded),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: '',
          ),
        ],
      ),
    );
  }
}
