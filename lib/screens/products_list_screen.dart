import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:store/models/product_model.dart';
import 'package:store/screens/page_views/product/product_item_screen.dart';

class ProductsListScreen extends StatefulWidget {
  const ProductsListScreen({Key? key}) : super(key: key);

  @override
  State<ProductsListScreen> createState() => _ProductsListScreenState();
}

class _ProductsListScreenState extends State<ProductsListScreen> {
  Future<List<ProductModel>> _loadProducts() async {
    Response response =
        await Dio().get('https://fakestoreapi.com/products');

    return List<ProductModel>.from(
      response.data.map((d) => ProductModel.fromJson(d)).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<ProductModel>>(
        future: _loadProducts(),
        builder: (context, AsyncSnapshot<List<ProductModel>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
              ),
            );
          }

          if (snapshot.hasError) {
            return Center(
              child: Text("Error: ${snapshot.error}"),
            );
          }

          final List<ProductModel> products = snapshot.data!;

          if (products.isEmpty) {
            return const Center(
              child: Text("No products found"),
            );
          }

          return ListView.builder(
            padding: EdgeInsets.zero,
            itemCount: products.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                child: ProductItemScreen(product:  products[index]),
              );
            },
          );
        },
      ),
    );
  }
}