import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:store/models/product_model.dart';
import 'package:store/screens/product_detail_screen.dart';

class ProductItemScreen extends StatefulWidget {
  final ProductModel product;

  const ProductItemScreen({Key? key, required this.product}) : super(key: key);

  @override
  State<ProductItemScreen> createState() => _ProductItemScreenState();
}

class _ProductItemScreenState extends State<ProductItemScreen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ProductDetailScreen(product: widget.product),
          ),
        );
      },
      child: Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              Image(
                height: 150.0,
                image: NetworkImage(widget.product.image),
                fit: BoxFit.cover,
              ),
              const SizedBox(height: 20.0),
              Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  color: Color(0xFFf6f6f6),
                ),
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.product.title,
                        style: const TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      Text(
                        NumberFormat.currency(
                          locale: "fr_FR",
                          symbol: "€",
                        ).format(widget.product.price),
                        style: const TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}