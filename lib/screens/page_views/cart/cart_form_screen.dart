import 'package:flutter/material.dart';
import 'package:store/tools/create_text_field.dart';

class CartFormScreen extends StatefulWidget {
  const CartFormScreen({Key? key}) : super(key: key);

  @override
  State<CartFormScreen> createState() => _CartFormScreenState();
}

class _CartFormScreenState extends State<CartFormScreen> {
  final _formKey = GlobalKey<FormState>();

  final streetController = TextEditingController();
  final zipController = TextEditingController();
  final cityController = TextEditingController();
  final countryController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CreateTextField.createTextField('Street', TextInputType.text, streetController),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.3,
                    child:  CreateTextField.createTextField('Zip', TextInputType.number, zipController),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child:  CreateTextField.createTextField('City', TextInputType.text, cityController),
                  ),
                ],
              ),
               CreateTextField.createTextField('Country', TextInputType.text, countryController),
            ],
          ),
        ),
      ),
    );
  }
}