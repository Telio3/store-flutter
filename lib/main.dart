import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:store/screens/home_screen.dart';
import 'package:store/services/cart_service.dart';
import 'package:store/services/liked_products_service.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Hive.initFlutter();

  LikedProductsService.box = await Hive.openBox('likedProducts');
  CartService.box = await Hive.openBox('cartProducts');

  runApp(const App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Store',
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
    );
  }
}
